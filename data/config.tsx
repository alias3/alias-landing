import { Link } from '@saas-ui/react';
import { NextSeoProps } from 'next-seo';
import { FaDribbble, FaGithub, FaInstagram, FaTwitter } from 'react-icons/fa';
import { FiCheck } from 'react-icons/fi';
import { Logo } from './logo';


const siteConfig = {
    logo: Logo,
    seo: {
        title: 'Alias Studio',
    } as NextSeoProps,
    termsUrl: '#',
    privacyUrl: '#',
    header: {
        links: [
            {
                id: 'login',
                label: 'Login',
                href: '/login',
            },
            {
                id: 'signup',
                label: 'Sign Up',
                href: '/signup',
                variant: 'primary',
            },
        ],
    },
    footer: {
        links: [
            {
                href: "/",
                label: <FaInstagram size="30" />
            },
            {
                href: "/",
                label: <FaDribbble size="29" />
            },
            {
                href: "/",
                label: <FaGithub size="29" />
            },
            {
                href: "/",
                label: <FaTwitter size="30" />
            }
        ],
        copyright: (
            <>
                © {new Date().getFullYear()} Alias. All rights reserved. <br />
                Built by&nbsp;
                <Link href="/" textDecoration="underline">Alien</Link>
                .
            </>
        ),
    },
    contact: {
        phoneNumber: "+33 012 345 6789",
        emailAddress: "hello@alias.studio"
    },
    signup: {
        title: 'Start building with Saas UI',
        features: [
            {
                icon: FiCheck,
                title: 'Accessible',
                description: 'All components strictly follow WAI-ARIA standards.',
            },
            {
                icon: FiCheck,
                title: 'Themable',
                description:
                    'Fully customize all components to your brand with theme support and style props.',
            },
            {
                icon: FiCheck,
                title: 'Composable',
                description:
                    'Compose components to fit your needs and mix them together to create new ones.',
            },
            {
                icon: FiCheck,
                title: 'Productive',
                description:
                    'Designed to reduce boilerplate and fully typed, build your product at speed.',
            },
        ],
    },
};

export default siteConfig;
