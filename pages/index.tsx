import * as React from 'react';
import type { NextPage } from 'next';
import Image from 'next/image';
import {
    Container,
    Box,
    Stack,
    HStack,
    ButtonGroup,
    Icon,
    Text,
    VStack,
    Grid
} from '@chakra-ui/react';
import { SEO } from 'components/seo/seo';

import { FallInPlace } from 'components/motion/fall-in-place';
import { Hero } from 'components/hero';
import { Br } from '@saas-ui/react';
import { Em } from 'components/typography';
import {
    FiArrowRight,
} from 'react-icons/fi';
import {
    LuRocket
} from 'react-icons/lu';
import { Features } from 'components/features';
import { BackgroundGradient } from 'components/gradients/background-gradient';

import { ButtonLink } from 'components/button-link/button-link';

import {
    Highlights,
    HighlightsItem
} from 'components/highlights';
import { ImageCard } from "components/imageCard/imageCard";
import { Section } from "components/section";
import Contact from "components/contact/contact";


const Home: NextPage = () => {
    return (
        <Box>
            <SEO
                title="Alias"
                description="A collaborative AI studio where you create with favorite artists in their iconic styles."
            />
            <Box>
                <HeroSection />

                <DescSection />

                <ForCreatorsSection />

                <JoinAMovementSection />

                <ForCollaboratorsSection />

                <RecentlyGeneratedSection />

                <Contact />
            </Box>
        </Box>
    );
};

const HeroSection: React.FC = () => {
    return (
        <Box position="relative" overflow="hidden">
            <BackgroundGradient height="100%" />
            <Container maxW="container.xl" pt={{ base: 40, lg: 60 }} pb="256px">
                <Stack alignItems="center">
                    <Hero
                        id="home"
                        display="flex"
                        justifyContent="center"
                        px="0"
                        title={
                            <FallInPlace textAlign="center" letterSpacing="2px" width="100%">
                                Create
                                <Br /> with your
                                <Br /> Favorites
                            </FallInPlace>
                        }
                        description={
                            <FallInPlace delay={0.4} fontWeight="medium" textAlign="center" pl="0">
                                <Em>Alias</Em> is a collaborative AI studio
                                <Br /> where you create with your
                                <Br /> favorite artists in their iconic styles.
                            </FallInPlace>
                        }
                    >
                        <FallInPlace delay={0.8}>
                            <HStack pt="4" pb="6" spacing="8">
                            </HStack>

                            <ButtonGroup
                                spacing={4}
                                alignItems="center"
                                width="100%"
                                display="flex"
                                justifyContent="center"
                            >
                                <ButtonLink
                                    size="lg"
                                    href="/"
                                    variant="solid"
                                    borderRadius="16px"
                                    colorScheme="green"
                                    leftIcon={
                                        <Icon
                                            as={LuRocket}
                                            sx={{
                                                transitionProperty: 'common',
                                                transitionDuration: 'normal'
                                            }}
                                        />
                                    }
                                >
                                    Start Creating
                                </ButtonLink>
                            </ButtonGroup>
                        </FallInPlace>
                    </Hero>
                </Stack>
            </Container>

            <Features
                id="benefits"
                innerWidth="container.xl"
                pt="0"
                title="Testimonials"
                features={[
                    {
                        title: 'Tòmas Saraceno',
                        subtitle: 'Contemporary artist',
                        description: `
                            Alias lets me collaborate with my fans through AI in a new and exciting way.
                            The platform has opened up a whole new world of possibilities for me as an artist,
                            allowing my community to re-imagine my art with the help of Alias last gen AI generative model!
                        `,
                        delay: 0.6
                    },
                    {
                        title: 'Michael L.',
                        subtitle: 'AI concept artist at night',
                        description: `
                            Alias is more than just a platform, it's a community of artists
                            and AI enthusiasts who support each other and inspire each other.
                            I highly recommend Alias to anyone who wants to
                            unleash their artistic potential with AI.
                        `,
                        delay: 0.8,
                    },
                    {
                        title: 'Lauren M.',
                        subtitle: 'Professional AI artist, open for freelance',
                        description: `
                            Thanks to Alias, I was able to create a stunning piece of art
                            that caught the attention of Ai Weiwei himself.
                            He loved it so much that he decided to use it for his own project,
                            I was retributed by the platform for my contribution.
                            This was a dream come true for me.
                        `,
                        delay: 1,
                    }
                ]}
                reveal={FallInPlace}
            />
        </Box>
    );
};

const DescSection: React.FC = () => {
    return (
        <FallInPlace delay={1} mb="3rem">
            <Box
                position="relative"
                overflow="hidden"
                width="100%"
                display="flex"
                justifyContent="center"
                flexDirection="column"
                alignItems="center"
                px="32px"
            >
                <FallInPlace delay={1} textAlign="center" pt="var(--chakra-space-28)" pb="0" px="32px">
                    <h1
                        style={{
                            fontFamily: "var(--chakra-fonts-heading)",
                            fontSize: "30px",
                            fontWeight: "var(--chakra-fontWeights-bold)"
                        }}
                    >
                        A collaborative and generative studio
                    </h1>
                </FallInPlace>
                <br />
                <Text color="muted" fontSize="xl" maxWidth="700px" textAlign="center">
                    Alias is a reimagination of the legendary ateliers of Da Vinci,
                    Rembrandt, Michelangelo, and other masters.
                    Our mission is to empower artists and brands to unleash
                    their creativity through generative AI models,
                    by enabling them to engage and create with their communities
                    from their unique generative AI studio.
                    <br />
                    <br />
                    We are building the creative studio of the AI age.
                </Text>
            </Box>
        </FallInPlace>
    );
};

const ForCreatorsSection: React.FC = () => {
    return (
        <>
            <FallInPlace delay={1} textAlign="center" pt="var(--chakra-space-28)" pb="32px" px="32px" mt="5rem">
                <h1
                    style={{
                        fontFamily: "var(--chakra-fonts-heading)",
                        fontSize: "30px",
                        fontWeight: "var(--chakra-fontWeights-bold)"
                    }}
                >
                    For Creators
                </h1>
            </FallInPlace>
            <Highlights pt="0" pb="48px">
                <FallInPlace delay={1}>
                    <HighlightsItem colSpan={[1]} title="Open your virtual studio">

                        <VStack alignItems="flex-start" spacing="4" >
                            <Text color="muted" fontSize="xl">
                                Train an AI on your unique artistic style & share it with a vibrant
                                creative community.
                            </Text>
                        </VStack>

                    </HighlightsItem>
                </FallInPlace>

                <FallInPlace delay={1.1}>
                    <HighlightsItem colSpan={[1]} title="Engage with your community">
                        <VStack alignItems="flex-start" spacing="4" >
                            <Text color="muted" fontSize="xl">
                                Spark collaboration to create innovative artwork that pushes your style&apos;s boundaries.
                            </Text>
                        </VStack>
                    </HighlightsItem>
                </FallInPlace>

                <FallInPlace delay={1.2}>
                    <HighlightsItem colSpan={[1]} title="Monetize your work">
                        <VStack alignItems="flex-start" spacing="4" >
                            <Text color="muted" fontSize="xl">
                                Get fairly compensated for any commercial utilization of your augmented body of work.
                            </Text>
                        </VStack>
                    </HighlightsItem>
                </FallInPlace>
            </Highlights>

            <FallInPlace delay={1}>
                <ButtonGroup
                    spacing={4}
                    alignItems="center"
                    width="100%"
                    display="flex"
                    justifyContent="center"
                >
                    <ButtonLink
                        size="lg"
                        href="/"
                        variant="solid"
                        colorScheme="green"
                        borderRadius="16px"
                        rightIcon={
                            <Icon
                                as={FiArrowRight}
                                sx={{
                                    transitionProperty: 'common',
                                    transitionDuration: 'normal',
                                    '.chakra-button:hover &': {
                                        transform: 'translate(5px)',
                                    },
                                }}
                            />
                        }
                    >
                        Open your studio
                    </ButtonLink>
                </ButtonGroup>
            </FallInPlace>
        </>
    );
};

const JoinAMovementSection: React.FC = () => {
    return (
        <>
            <FallInPlace
                delay={1}
                textAlign="center"
                display="flex"
                flexDirection="column"
                alignItems="center"
                pt="var(--chakra-space-28)"
                pb="32px"
                px="32px"
                mt="5rem"
            >
                <h1
                    style={{
                        fontFamily: "var(--chakra-fonts-heading)",
                        fontSize: "30px",
                        fontWeight: "var(--chakra-fontWeights-bold)"
                    }}
                >
                    Join a Movement
                </h1>
                <Section
                    innerWidth="container.xl"
                    position="relative"
                    overflow="hidden"
                    pt="2rem"
                    pb="0"
                    px="0"
                >
                    <Grid
                        templateColumns={{ base: 'repeat(1, 1fr)', lg: 'repeat(3, 1fr)' }}
                        gap={8}
                        position="relative"
                    >
                        <ImageCard
                            image="/static/cards/nike.png"
                            title="Nike"
                            collaborateLink="/"
                        />
                        <ImageCard
                            image="/static/cards/tomas.png"
                            title="Tòmas Saraceno"
                            collaborateLink="/"
                        />
                        <ImageCard
                            image="/static/cards/basquiat.jpg"
                            title="Basquiat"
                            collaborateLink="/"
                        />
                    </Grid>
                </Section>
            </FallInPlace>
        </>
    );
};

const ForCollaboratorsSection: React.FC = () => {
    return (
        <>
            <FallInPlace delay={1} textAlign="center" pt="var(--chakra-space-28)" pb="32px" px="32px" mt="3rem">
                <h1
                    style={{
                        fontFamily: "var(--chakra-fonts-heading)",
                        fontSize: "30px",
                        fontWeight: "var(--chakra-fontWeights-bold)"
                    }}
                >
                    For Collaborators
                </h1>
            </FallInPlace>
            <Highlights pt="0" pb="48px">
                <FallInPlace delay={1}>
                    <HighlightsItem colSpan={[1]} title="Engage with artists">

                        <VStack alignItems="flex-start" spacing="4" >
                            <Text color="muted" fontSize="xl">
                                Remix the styles and works of your favorite artists
                                to produce your own original art pieces.
                            </Text>
                        </VStack>

                    </HighlightsItem>
                </FallInPlace>

                <FallInPlace delay={1.1}>
                    <HighlightsItem colSpan={[1]} title="Get paid for your work">
                        <VStack alignItems="flex-start" spacing="4" >
                            <Text color="muted" fontSize="xl">
                                Receive fair compensation when your creations
                                are used commercially or used as the source
                                for someone else&apos;s work.
                            </Text>
                        </VStack>
                    </HighlightsItem>
                </FallInPlace>

                <FallInPlace delay={1.2}>
                    <HighlightsItem colSpan={[1]} title="Socialize with a like-minded community">
                        <VStack alignItems="flex-start" spacing="4" >
                            <Text color="muted" fontSize="xl">
                                Exchange ideas, techniques, and inspiration with those
                                who appreciate the same artists as you.
                            </Text>
                        </VStack>
                    </HighlightsItem>
                </FallInPlace>
            </Highlights>

            <FallInPlace delay={1}>
                <ButtonGroup
                    spacing={4}
                    alignItems="center"
                    width="100%"
                    display="flex"
                    justifyContent="center"
                >
                    <ButtonLink
                        size="lg"
                        href="/"
                        variant="solid"
                        colorScheme="green"
                        borderRadius="16px"
                        rightIcon={
                            <Icon
                                as={FiArrowRight}
                                sx={{
                                    transitionProperty: 'common',
                                    transitionDuration: 'normal',
                                    '.chakra-button:hover &': {
                                        transform: 'translate(5px)',
                                    },
                                }}
                            />
                        }
                    >
                        Generate an image
                    </ButtonLink>
                </ButtonGroup>
            </FallInPlace>
        </>
    );
};

const RecentlyGeneratedSection: React.FC = () => {
    return (
        <FallInPlace
            delay={1}
            textAlign="center"
            pt="var(--chakra-space-28)"
            pb="32px"
            px="32px"
            mt="4rem"
            display="flex"
            flexDirection="column"
            alignItems="center"
        >
            <h1
                style={{
                    fontFamily: "var(--chakra-fonts-heading)",
                    fontSize: "30px",
                    fontWeight: "var(--chakra-fontWeights-bold)"
                }}
            >
                Recently Generated
            </h1>
            <Section
                innerWidth="container.xl"
                position="relative"
                overflow="hidden"
                pt="2rem"
                pb="0"
                px="0"
            >
                <Grid
                    templateColumns={{ base: 'repeat(1, 1fr)', lg: 'repeat(3, 1fr)' }}
                    gap={8}
                    position="relative"
                >
                    <ImageCard
                        image="/static/recentlyGenerated/sunnyHotel.png"
                        title="Sunny hotel interior"
                        alignTitleCenter={true}
                        model="Orient Express"
                        modelLogo="/static/models/orientExpress.png"
                        author="LunaSketch"
                    />
                    <ImageCard
                        image="/static/recentlyGenerated/theSpiderProject.png"
                        title="The Spider project"
                        alignTitleCenter={true}
                        model="Tòmas Saraceno"
                        modelLogo="/static/models/tomasSaraceno.jpg"
                        author="ArtBySimon"
                    />
                    <ImageCard
                        image="/static/recentlyGenerated/myLoveLetterToNike.png"
                        title="My love letter to Nike"
                        alignTitleCenter={true}
                        model="Nike"
                        modelLogo="/static/models/nike.png"
                        author="Nike99ever"
                    />
                </Grid>
            </Section>
        </FallInPlace>
    );
};

export default Home;