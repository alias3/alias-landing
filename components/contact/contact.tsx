import { Box, ButtonGroup, HStack, Icon, Input, InputGroup, InputLeftElement, Link } from "@chakra-ui/react";
import { Card } from "@saas-ui/react";
import { FooterLink } from "components/layout/footer";
import { FallInPlace } from "components/motion/fall-in-place";
import siteConfig from "data/config";
import { FiMail, FiPhoneCall, FiUser } from "react-icons/fi";
import { Text } from '@chakra-ui/react';
import { ButtonLink } from "components/button-link";


export default function Contact() {
    return (
        <FallInPlace
            delay={1}
            textAlign="center"
            pt="var(--chakra-space-28)"
            width="100%"
            display="flex"
            alignItems="center"
            flexDirection="column"
            px="32px"
            mt="4rem"
        >
            <Box mb="11rem" mx="32px">
                <h1
                    style={{
                        fontFamily: "var(--chakra-fonts-heading)",
                        fontSize: "30px",
                        fontWeight: "var(--chakra-fontWeights-bold)"
                    }}
                >
                    Pricing
                </h1>
                <Text fontSize="md" pb="3px" pl="6px" maxW="300px">
                    If you are an artist or brand looking to
                    have images generated in an Alias of your style,
                    please contact us&nbsp;
                    <Link
                        href={`mailto:${siteConfig.contact.emailAddress}`}
                        textDecoration="underline"
                    >
                        here
                    </Link>.
                </Text>
            </Box>

            <Card
                borderRadius="16px"
                p="32px"
                m="0"
                flex="1 0"
                maxW="600px"
                alignItems="flex-start"
                overflow="hidden"
                position="relative"
                bgColor="white"
                opacity="1"
            >
                <h1
                    style={{
                        fontFamily: "var(--chakra-fonts-heading)",
                        fontSize: "30px",
                        fontWeight: "var(--chakra-fontWeights-bold)"
                    }}
                >
                    Let&apos;s get in touch!
                </h1>
                <p
                    style={{
                        fontFamily: "var(--chakra-fonts-heading)",
                        fontSize: "1rem",
                        fontWeight: "400",
                        marginTop: "1rem"
                    }}
                >
                    Got questions about Alias? Our team is here to help.
                </p>

                <Box width="100%" display="flex" justifyContent="center" mt="3rem">
                    <Box
                        width="auto"
                        display="flex"
                        flexDirection="column"
                        alignContent="flex-start"
                    >
                        <HStack>
                            <FiPhoneCall size="1.2rem" />
                            <Text fontSize="md" pb="3px" pl="4px">
                                {siteConfig.contact.phoneNumber}
                            </Text>
                        </HStack>
                        <HStack mt="0.6rem">
                            <FiMail size="1.2rem" />
                            <Text fontSize="md" pb="3px" pl="6px">
                                {siteConfig.contact.emailAddress}
                            </Text>
                        </HStack>
                    </Box>
                </Box>

                <Box width="100%" display="flex" justifyContent="center" mt="1.8rem">
                    <Box
                        width="min-content"
                        display="flex"
                        flexDirection="column"
                        alignContent="flex-start"
                    >
                        <h3
                            style={{
                                fontFamily: "var(--chakra-fonts-heading)",
                                fontSize: "1.5rem",
                                fontWeight: "600",
                                marginTop: "1rem"
                            }}
                        >
                            Connect with us
                        </h3>

                        <HStack justify="flex-start" spacing="4" mt="1rem" mb="19px" mr="2rem" pl="5px">
                            {siteConfig.footer?.links?.map(({ href, label }) => (
                                <FooterLink key={href} href={href}>
                                    {label}
                                </FooterLink>
                            ))}
                        </HStack>
                    </Box>
                </Box>

                <Box width="100%" display="flex" alignItems="center" flexDirection="column" mt="1.8rem">
                    <InputGroup size="lg" maxW="300px">
                        <InputLeftElement
                            pointerEvents="none"
                        >
                            <FiUser color="gray.300" />
                        </InputLeftElement>
                        <Input placeholder='Full Name' type="text" />
                    </InputGroup>

                    <InputGroup size="lg" maxW="300px" mt="0.8rem">
                        <InputLeftElement
                            pointerEvents="none"
                        >
                            <FiMail color="gray.300" />
                        </InputLeftElement>
                        <Input placeholder='Email' type="text" />
                    </InputGroup>

                    <ButtonGroup
                        spacing={4}
                        alignItems="center"
                        width="100%"
                        display="flex"
                        justifyContent="center"
                        mt="1.8rem"
                    >
                        <ButtonLink
                            size="lg"
                            href="/"
                            variant="solid"
                            colorScheme="green"
                            borderRadius="16px"
                            minW="220px"
                        >
                            Submit
                        </ButtonLink>
                    </ButtonGroup>
                </Box>
            </Card>
        </FallInPlace >
    );
}