import {
    Box,
    BoxProps,
    SimpleGrid,
    Container,
    Text,
    Stack,
    Flex,
    HStack,
} from '@chakra-ui/react';

import { Link, LinkProps } from '@saas-ui/react';

import siteConfig from 'data/config';

export interface FooterProps extends BoxProps {
    columns?: number;
}

export const Footer: React.FC<FooterProps> = (props) => {
    const { columns = 2, ...rest } = props;
    return (
        <Box bg="white" _dark={{ bg: 'gray.900' }} {...rest}>
            <Container
                maxW="container.2xl"
                px="2rem"
                py="0"
                mt="12rem"
                mb="2rem"
            >
                <Box
                    display="flex"
                    flexDirection="row"
                    justifyContent="space-around"
                    alignItems="flex-start"
                    flexWrap="wrap-reverse"
                    mb="2rem"
                >
                    <Box
                        mt="1rem"
                        mr="1rem"
                    >
                        <Stack
                            alignItems="flex-start"
                            mb="32px"
                            ml="3px"
                        >
                            <Box
                                as={siteConfig.logo}
                                flex="1"
                                width="96px"
                                height="auto"
                                overflow="visible"
                                mt="-56px"
                                mb="40px"
                                ml="27px"
                            />

                            <Text fontSize="md" color="muted">
                                {siteConfig.seo.description}
                            </Text>
                        </Stack>
                        <Copyright>{siteConfig.footer.copyright}</Copyright>
                    </Box>

                    <HStack
                        justify="flex-end"
                        spacing="4"
                        alignSelf="center"
                        minW="100px"
                        mt="1rem"
                        ml="1rem"
                    >
                        {siteConfig.footer?.links?.map(({ href, label }) => (
                            <FooterLink key={href} href={href}>
                                {label}
                            </FooterLink>
                        ))}
                    </HStack>
                </Box>
            </Container>
        </Box>
    );
};

export interface CopyrightProps {
    title?: React.ReactNode;
    children: React.ReactNode;
}

export const Copyright: React.FC<CopyrightProps> = ({
    title,
    children,
}: CopyrightProps) => {
    let content;
    if (title && !children) {
        content = `&copy; ${new Date().getFullYear()} - ${title}`;
    }
    return (
        <Text color="muted" fontSize="sm">
            {content || children}
        </Text>
    );
};

export const FooterLink: React.FC<LinkProps> = (props) => {
    const { children, ...rest } = props;
    return (
        <Link
            color="muted"
            fontSize="sm"
            textDecoration="none"
            _hover={{
                color: 'white',
                transition: 'color .2s ease-in',
            }}
            {...rest}
        >
            {children}
        </Link>
    );
};
