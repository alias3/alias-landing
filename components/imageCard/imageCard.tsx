import {
    Box,
    ButtonGroup,
    Grid,
    GridItem,
    GridItemProps,
    Heading,
    Icon,
    Text,
    useColorModeValue,
    useTheme,
    VStack,
} from '@chakra-ui/react';
import { Card, CardMedia, CardProps, Link } from '@saas-ui/react';
import { Section, SectionProps } from 'components/section';
import { Testimonial, TestimonialProps } from 'components/testimonials';
import { ButtonLink } from 'components/button-link/button-link';
import { FiArrowRight } from "react-icons/fi";


export interface ImageCardProps extends CardProps {
    image?: string;
    alignTitleCenter?: boolean;
    collaborateLink?: string;
    model?: string;
    modelLogo?: string;
    author?: string;
}


export const ImageCard: React.FC<ImageCardProps> = (props) => {
    const {
        children,
        title,
        image,
        alignTitleCenter,
        collaborateLink,
        model,
        modelLogo,
        author,
        ...rest
    } = props;

    return (
        <Card
            borderRadius="16px"
            justifySelf="center"
            width="280px"
            p="0"
            m="0"
            flex="1 0"
            alignItems="flex-start"
            spacing="8"
            overflow="hidden"
            position="relative"
            bgColor="white"
            opacity="1"
            {...rest}
        >
            {image && (
                <CardMedia
                    height="260px"
                    bgImage={image}
                    borderRadius="0"
                    bgSize="cover"
                    bgRepeat="no-repeat"
                    bgPos="center"
                />
            )}
            <Box
                my="2rem"
                width="100%"
                display="flex"
                flexDirection="column"
                alignItems="center"
                flexWrap="wrap"
            >
                <Box mb="1rem">
                    {title && (
                        <Heading fontSize="3xl" textAlign={alignTitleCenter ? "center" : "left"}>
                            {title}
                        </Heading>
                    )}
                </Box>

                <Box width="100%" display="flex" flexDirection="column" alignItems="center">
                    {/* Collaborator btn */}
                    {collaborateLink && (
                        <Box>
                            <ButtonGroup
                                spacing={4}
                                alignItems="center"
                                display="flex"
                                justifyContent="center"
                            >
                                <ButtonLink
                                    size="lg"
                                    href={collaborateLink}
                                    variant="solid"
                                    colorScheme="green"
                                    borderRadius="16px"
                                    leftIcon={
                                        <Icon
                                            as={FiArrowRight}
                                            sx={{
                                                transitionProperty: 'common',
                                                transitionDuration: 'normal'
                                            }}
                                        />
                                    }
                                    rightIcon={
                                        <Icon
                                            as={FiArrowRight}
                                            sx={{
                                                transitionProperty: 'common',
                                                transitionDuration: 'normal'
                                            }}
                                        />
                                    }
                                >
                                    Collaborate
                                </ButtonLink>
                            </ButtonGroup>
                        </Box>
                    )}

                    {/* Model Logo */}
                    {modelLogo && (
                        <Box width="150px" height="auto" px="2rem">
                            <Box
                                as="img"
                                src={modelLogo}
                                alt="model logo"
                                width="100%"
                                borderRadius="4px"
                                aspectRatio="1/1"
                                objectFit="contain"
                            />
                        </Box>
                    )}

                    {/* Model */}
                    {model && (
                        <Box width="280px" px="2rem">
                            <Text fontSize="xl" textAlign="left">
                                <u>Model:</u> {model}
                            </Text>
                        </Box>
                    )}

                    {/* Author */}
                    {author && (
                        <Box mt="1rem" width="280px" px="2rem">
                            <Text fontSize="md" textAlign="left">
                                {author}
                            </Text>
                        </Box>
                    )}
                </Box>
            </Box>
        </Card>
    );
};