import {
    VStack,
    Heading,
    Box,
    StackProps,
    useMultiStyleConfig,
} from '@chakra-ui/react';

export interface SectionTitleProps extends Omit<StackProps, 'title'> {
    title: React.ReactNode;
    subtitle?: React.ReactNode;
    description?: React.ReactNode;
    align?: 'left' | 'center';
    variant?: string;
}

export const SectionTitle: React.FC<SectionTitleProps> = (props) => {
    const { title, description, align, variant, ...rest } = props;
    const styles = useMultiStyleConfig('SectionTitle', { variant });

    return (
        <VStack
            sx={styles.wrapper}
            alignItems={align === 'left' ? 'flex-start' : 'center'}
            spacing={4}
            {...rest}
        >
            <h1
                style={{
                    fontFamily: "var(--chakra-fonts-heading)",
                    fontSize: "30px",
                    fontWeight: "var(--chakra-fontWeights-bold)"
                }}
            >
                {title}
            </h1>
            {description && (
                <Box sx={styles.description} textAlign={align}>
                    {description}
                </Box>
            )}
        </VStack>
    );
};
